#include <msgbus/handler.hpp>
#include <msgbus/workers.hpp>
#include <msgbus/service.hpp>
#include <msgbus/msgbus.hpp>

#include <cg/component.hpp>
#include <cg/graph.hpp>

#include <tester/tester.hpp>
#include <set>

BOOST_AUTO_TEST_SUITE(Volatile)

struct Component : cg::Component
{
    struct T
    {
        template<typename Base>
        struct Interface : Base
        {
            auto handler()
            {
                return msgbus::Handler
                {
                    msgbus::Event
                    {
                        [this]{
                            return this->state.interval;
                        },
                        [this](auto &lock) {
                            this->state.ticks.insert(std::chrono::time_point_cast<std::chrono::seconds>(
                                std::chrono::system_clock::now()
                            ).time_since_epoch().count() - this->state.started.count());
                        }
                    }
                };
            }
        };
    };

    struct D
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                using namespace std::chrono_literals;

                std::this_thread::sleep_for(4s);
                this->state.notify([this]{
                    this->state.interval = 2s;
                });
                std::this_thread::sleep_for(5s);
                this->state.notify([this]{
                    this->state.interval = std::chrono::seconds::max();
                });
                std::this_thread::sleep_for(4s);

                this->state.stop();
                return 0;
            }
        };
    };

    template<typename>
    struct State : msgbus::State<T>
    {
        std::set<std::uint32_t> &ticks;

        std::chrono::seconds interval = std::chrono::seconds::max();
        std::chrono::seconds const started{std::chrono::time_point_cast<std::chrono::seconds>(
            std::chrono::system_clock::now()
        ).time_since_epoch().count()};

        State(std::set<std::uint32_t> &ticks) : ticks{ticks} {}
    };

    using Services = ct::join_t<ct::tuple<D>, msgbus::Workers<T>>;
};

BOOST_AUTO_TEST_CASE(Test)
{
    std::set<std::uint32_t> ticks;
    using G = cg::Graph<msgbus::Clients<Component>>;

    auto const t1 = std::chrono::time_point_cast<std::chrono::seconds>(std::chrono::system_clock::now());
    BOOST_TEST(0 == msgbus::Service<G>(
        cg::Args<Component>(ticks)
    ));
    auto const t2 = std::chrono::time_point_cast<std::chrono::seconds>(std::chrono::system_clock::now());

    auto const expected = std::set<std::uint32_t>{6, 8};
    BOOST_TEST(expected == ticks);
    BOOST_TEST(13 <= (t2 - t1).count());
}

BOOST_AUTO_TEST_SUITE_END()
