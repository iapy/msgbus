#include <msgbus/handler.hpp>
#include <tester/tester.hpp>

namespace {

struct Fixture
{
    std::unique_lock<std::mutex> dummy;
    std::vector<int> values{0, 0, 0, 0, 0, 0};
};

} // namespace

BOOST_FIXTURE_TEST_SUITE(Handler, Fixture)

BOOST_AUTO_TEST_CASE(Single)
{
    auto handler = msgbus::Handler
    {
        msgbus::Event
        {
            [this]{ return Fixture::values[0] != 0; },
            [this](auto &) { Fixture::values[1] = 42; }
        }
    };

    decltype(handler)::Bitset bitset = 0;

    handler.wait(bitset);
    BOOST_TEST(!bitset.any());

    handler.handle(bitset, dummy, false);
    BOOST_TEST(0 == Fixture::values[1]);
    BOOST_TEST(!bitset.any());

    values[0] = 10;

    handler.wait(bitset);
    BOOST_TEST(bitset.any());

    handler.handle(bitset, dummy, false);
    BOOST_TEST(42 == Fixture::values[1]);
    BOOST_TEST(!bitset.any());
}

BOOST_AUTO_TEST_CASE(Multiple)
{
    auto handler = msgbus::Handler
    {
        msgbus::Event
        {
            [this]{ return Fixture::values[0] != 0; },
            [this](auto &) { Fixture::values[1] = 42; }
        },
        msgbus::Event
        {
            [this]{ return Fixture::values[2] != 0; },
            [this](auto &) { Fixture::values[3] = 24; }
        }
    };

    decltype(handler)::Bitset bitset = 0;

    handler.wait(bitset);
    BOOST_TEST(!bitset.any());

    handler.handle(bitset, dummy, false);
    BOOST_TEST(0 == Fixture::values[1]);
    BOOST_TEST(0 == Fixture::values[3]);
    BOOST_TEST(!bitset.any());

    values[0] = 1;

    handler.wait(bitset);
    BOOST_TEST(bitset == 1);

    handler.handle(bitset, dummy, false);
    BOOST_TEST(42 == Fixture::values[1]);
    BOOST_TEST(0  == Fixture::values[3]);
    BOOST_TEST(!bitset.any());

    values[2] = 1;

    handler.wait(bitset);
    BOOST_TEST(bitset == 3);

    handler.handle(bitset, dummy, false);
    BOOST_TEST(42 == Fixture::values[1]);
    BOOST_TEST(24 == Fixture::values[3]);
    BOOST_TEST(!bitset.any());
}

BOOST_AUTO_TEST_CASE(ConstantTimeoutSingle)
{
    auto handler = msgbus::Handler
    {
        msgbus::Event
        {
            std::chrono::seconds(1),
            [this](auto &) { Fixture::values[1] = 42; }
        }
    };

    decltype(handler)::Bitset bitset = 0;
    static_assert( decltype(handler)::constant_timeout);
    static_assert(!decltype(handler)::volatile_timeout);

    BOOST_TEST(1 == handler.timeout().count());

    handler.wait(bitset);
    BOOST_TEST(!bitset.any());

    handler.handle(bitset, dummy, false);
    BOOST_TEST(0 == Fixture::values[1]);
    BOOST_TEST(!bitset.any());

    handler.handle(bitset, dummy, true);
    BOOST_TEST(42 == Fixture::values[1]);
    BOOST_TEST(!bitset.any());
}

BOOST_AUTO_TEST_CASE(VolatileTimeoutSingle)
{
    auto handler = msgbus::Handler
    {
        msgbus::Event
        {
            [this] { return std::chrono::seconds(Fixture::values[0]); },
            [this](auto &) { Fixture::values[1] = 42; }
        }
    };

    decltype(handler)::Bitset bitset = 0;
    static_assert(!decltype(handler)::constant_timeout);
    static_assert( decltype(handler)::volatile_timeout);

    BOOST_TEST(0 == handler.timeout().count());

    Fixture::values[0] = 10;
    BOOST_TEST(10 == handler.timeout().count());

    handler.wait(bitset);
    BOOST_TEST(!bitset.any());

    handler.handle(bitset, dummy, false);
    BOOST_TEST(0 == Fixture::values[1]);
    BOOST_TEST(!bitset.any());

    handler.handle(bitset, dummy, true);
    BOOST_TEST(42 == Fixture::values[1]);
    BOOST_TEST(!bitset.any());
}

BOOST_AUTO_TEST_CASE(ConstantTimeoutMultiple)
{
    auto handler = msgbus::Handler
    {
        msgbus::Event
        {
            std::chrono::seconds(1),
            [this](auto &) { Fixture::values[1] = 42; }
        },
        msgbus::Event
        {
            [this]{ return Fixture::values[0] != 0; },
            [this](auto &) { Fixture::values[2] = 42; }
        },
        msgbus::Event
        {
            [this]{ return Fixture::values[3] != 0; },
            [this](auto &) { Fixture::values[4] = 24; }
        }
    };

    decltype(handler)::Bitset bitset = 0;
    static_assert( decltype(handler)::constant_timeout);
    static_assert(!decltype(handler)::volatile_timeout);

    BOOST_TEST(1 == handler.timeout().count());

    handler.wait(bitset);
    BOOST_TEST(!bitset.any());

    handler.handle(bitset, dummy, false);
    BOOST_TEST(0 == Fixture::values[1]);
    BOOST_TEST(!bitset.any());

    handler.handle(bitset, dummy, true);
    BOOST_TEST(42 == Fixture::values[1]);
    BOOST_TEST(!bitset.any());

    Fixture::values[0] = 1;

    handler.wait(bitset);
    BOOST_TEST(bitset == 1);

    handler.handle(bitset, dummy, false);
    BOOST_TEST(42 == Fixture::values[2]);
    BOOST_TEST( 0 == Fixture::values[4]);
    BOOST_TEST(!bitset.any());

    Fixture::values[2] = 0;
    Fixture::values[3] = 1;

    handler.wait(bitset);
    BOOST_TEST(bitset == 3);

    handler.handle(bitset, dummy, false);
    BOOST_TEST(42 == Fixture::values[2]);
    BOOST_TEST(24 == Fixture::values[4]);
    BOOST_TEST(!bitset.any());
}

BOOST_AUTO_TEST_CASE(VolatileTimeoutMultiple)
{
    auto handler = msgbus::Handler
    {
        msgbus::Event
        {
            [this]{ return Fixture::values[0] != 0; },
            [this](auto &) { Fixture::values[2] = 42; }
        },
        msgbus::Event
        {
            [this] { return std::chrono::seconds(Fixture::values[5]); },
            [this](auto &) { Fixture::values[1] = 42; }
        },
        msgbus::Event
        {
            [this]{ return Fixture::values[3] != 0; },
            [this](auto &) { Fixture::values[4] = 24; }
        }
    };

    decltype(handler)::Bitset bitset = 0;
    static_assert(!decltype(handler)::constant_timeout);
    static_assert( decltype(handler)::volatile_timeout);

    BOOST_TEST(0 == handler.timeout().count());
    
    Fixture::values[5] = 10;
    BOOST_TEST(10 == handler.timeout().count());

    Fixture::values[0] = 1;

    handler.wait(bitset);
    BOOST_TEST(bitset == 1);

    handler.handle(bitset, dummy, false);
    BOOST_TEST(42 == Fixture::values[2]);
    BOOST_TEST( 0 == Fixture::values[1]);
    BOOST_TEST( 0 == Fixture::values[4]);
    BOOST_TEST(!bitset.any());

    handler.handle(bitset, dummy, true);
    BOOST_TEST(42 == Fixture::values[2]);
    BOOST_TEST(42 == Fixture::values[1]);
    BOOST_TEST( 0 == Fixture::values[4]);
    BOOST_TEST(!bitset.any());
}

BOOST_AUTO_TEST_SUITE_END()
