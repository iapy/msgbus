import subprocess
import unittest
import signal
import time
import os

class Signal(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.APP = os.path.join(
            os.path.dirname(
                os.path.dirname(__file__)
            ), "tests", "test_app")

    def _signal(self, s):
        p = subprocess.Popen([self.APP], stdout=subprocess.PIPE)
        time.sleep(6)
        os.kill(p.pid, s)
        p.wait()
        self.assertEqual("tick=1\ntick=2\ntick=3\n", p.stdout.read().decode('utf8'))
        self.assertEqual(0, p.returncode)
        p.stdout.close()

    def test_sigterm(self):
        self._signal(signal.SIGTERM)

    def test_sigint(self):
        self._signal(signal.SIGINT)

    def test_sigusr1(self):
        self._signal(signal.SIGUSR1)

if __name__ == '__main__':
    unittest.main()
