#include <msgbus/handler.hpp>
#include <msgbus/workers.hpp>
#include <msgbus/service.hpp>
#include <msgbus/msgbus.hpp>

#include <cg/component.hpp>
#include <cg/graph.hpp>

struct Component : cg::Component
{
    struct T
    {
        template<typename Base>
        struct Interface : Base
        {
            auto handler()
            {
                return msgbus::Handler
                {
                    msgbus::Event
                    {
                        std::chrono::seconds(2),
                        [this](auto &) { std::cout << "tick=" << ++(this->state.tick) << '\n'; }
                    }
                };
            }
        };
    };

    template<typename>
    struct State : msgbus::State<T>
    {
        std::size_t tick{0};
    };

    using Services = msgbus::Workers<T>;
};

int main()
{
    return msgbus::Service<cg::Graph<msgbus::Clients<Component>>>();
}

