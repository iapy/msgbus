#include <msgbus/handler.hpp>
#include <msgbus/workers.hpp>
#include <msgbus/service.hpp>
#include <msgbus/msgbus.hpp>

#include <cg/component.hpp>
#include <cg/graph.hpp>

#include <tester/tester.hpp>
#include <boost/mpl/list.hpp>
#include <set>

BOOST_AUTO_TEST_SUITE(Ticker)

template<std::size_t Interval>
struct Component : cg::Component
{
    struct T
    {
        template<typename Base>
        struct Interface : Base
        {
            auto handler()
            {
                return msgbus::Handler
                {
                    msgbus::Event
                    {
                        std::chrono::seconds(Interval),
                        [this](auto &lock) {
                            auto now = std::chrono::time_point_cast<std::chrono::seconds>(
                                std::chrono::system_clock::now()
                            );
                            this->state.ticks.insert(now.time_since_epoch().count() % 60);

                            if(++this->state.count == (60 / Interval))
                            {
                                this->state.set(msgbus::Stopping);
                            }
                        }
                    }
                };
            }
        };
    };

    template<typename>
    struct State : msgbus::State<T>
    {
        std::set<std::uint32_t> &ticks;
        State(std::set<std::uint32_t> &ticks) : ticks{ticks} {}

        std::size_t count{0};
    };

    using Services = msgbus::Workers<T>;
};

using Intervals = boost::mpl::list<
    std::integral_constant<std::size_t, 10>,
    std::integral_constant<std::size_t, 15>,
    std::integral_constant<std::size_t, 20>,
    std::integral_constant<std::size_t, 30>
>;

BOOST_AUTO_TEST_CASE_TEMPLATE(Test, T, Intervals)
{
    std::set<std::uint32_t> ticks;
    using G = cg::Graph<msgbus::Clients<Component<T::value>>>;

    auto const t1 = std::chrono::time_point_cast<std::chrono::minutes>(std::chrono::system_clock::now());
    BOOST_TEST(0 == msgbus::Service<G>(
        cg::Args<Component<T::value>>(ticks)
    ));
    auto const t2 = std::chrono::time_point_cast<std::chrono::minutes>(std::chrono::system_clock::now());

    BOOST_TEST(1 == (t2.time_since_epoch().count() - t1.time_since_epoch().count()));

    auto expected = std::set<std::uint32_t>{};
    for(std::size_t i = 0; i < 60; i += T::value)
    {
        expected.insert(i);
    }
    BOOST_TEST(expected == ticks);
}

BOOST_AUTO_TEST_SUITE_END()
