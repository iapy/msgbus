#include <msgbus/handler.hpp>
#include <msgbus/workers.hpp>
#include <msgbus/service.hpp>
#include <msgbus/msgbus.hpp>

#include <cg/component.hpp>
#include <cg/graph.hpp>

#include <tester/tester.hpp>
#include <set>

BOOST_AUTO_TEST_SUITE(Events)

template<std::size_t N>
struct Component : cg::Component
{
    struct T
    {
        template<typename Base>
        struct Interface : Base
        {
            auto handler()
            {
                return handler(std::make_index_sequence<N>());
            }
        private:
            template<std::size_t ...I>
            auto handler(std::index_sequence<I...>)
            {
                return msgbus::Handler
                {
                    msgbus::Event
                    {
                        [this] {
                            return this->state.fired[I];
                        },
                        [this] (auto &lock) {
                            this->state.received[I] = true;
                        }
                    }...
                };
            }
        };
    };

    struct D
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                for(std::size_t i = 0; i < N; ++i)
                {
                    std::this_thread::sleep_for(std::chrono::seconds(1));
                    this->state.notify([i, this]{
                        BOOST_TEST(i == this->state.received.count());
                        this->state.fired[i] = true;
                    });
                }
                this->state.stop();
                return 0;
            }
        };
    };

    using Events = std::bitset<N>;

    template<typename>
    struct State : msgbus::State<T>
    {
        State(Events &received) : received{received}
        {
        }

        Events &received;
        Events fired{0};
    };

    using Services = ct::join_t<ct::tuple<D>, msgbus::Workers<T>>;
};

BOOST_AUTO_TEST_CASE(Test)
{
    Component<10>::Events received;

    using G = cg::Graph<msgbus::Clients<Component<10>>>;
    BOOST_TEST(0 == msgbus::Service<G>(
        cg::Args<Component<10>>(received)
    ));
    BOOST_TEST(received.all());
}

BOOST_AUTO_TEST_SUITE_END()
