#pragma once
#include <cg/service.hpp>
#include <thread>
#include <csignal>

namespace msgbus {

template<typename Graph, typename ...Args>
int Service(Args&&... args)
{
    int rc;

    auto handler = [](int signal) {
        if(signal == SIGTERM)
        {
            exit(0);
        }
    };

    ::signal(SIGINT,  handler);
    ::signal(SIGUSR1, handler);
    ::signal(SIGTERM, handler);

    std::mutex mu;
    std::condition_variable cv;
    std::function<void(std::unique_lock<std::mutex>&)> stop = nullptr;

    cg::Host<Graph> host{
        std::forward<Args>(args)...,
        cg::Args<msgbus::Msgbus>(&mu, &cv, &stop),
    };
    std::thread service{[&]() mutable {
        rc = cg::Service(host);
    }};

    ::sigset_t mask;
    ::sigemptyset (&mask);
    ::sigaddset   (&mask, SIGINT);
    ::sigaddset   (&mask, SIGUSR1);

    int signal;
    if(sigwait(&mask, &signal) == 0)
    {
        std::unique_lock<std::mutex> lock(mu);
        cv.wait(lock, [&]{
            return stop != nullptr;
        });
        stop(lock);
        service.join();
    }

    return rc;
}

} // namespace msgbus
