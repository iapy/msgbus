#pragma once
#include <msgbus/detail/traits.hpp>
#include <ct/filter.hpp>
#include <ct/tuple.hpp>

#include <mutex>
#include <bitset>
#include <utility>

namespace msgbus {

template<typename C, typename A>
struct Event
{
    C cond;
    A action;

    static constexpr bool constant_timeout = detail::is_duration_v<C>;
    static constexpr bool volatile_timeout = detail::is_duration_calculator_v<C>;
};

template<typename ...Es>
class Handler : Es...
{
    using Events = ct::tuple<Es...>;
    static_assert(ct::sum(std::integer_sequence<int, Es::constant_timeout...>{}) + ct::sum(std::integer_sequence<int, Es::volatile_timeout...>{}) <= 1);
public:
    Handler(Es&& ...es) : Es(std::forward<Es>(es))... {}

    static constexpr bool constant_timeout = ct::sum(std::integer_sequence<int, Es::constant_timeout...>{});
    static constexpr bool volatile_timeout = ct::sum(std::integer_sequence<int, Es::volatile_timeout...>{});

    using Bitset = std::bitset<(sizeof... (Es)) - (constant_timeout || volatile_timeout)>;

    auto timeout() const
    {
        if constexpr (constant_timeout)
        {
            return ct::get_t<ct::index<0>, ct::filter_t<detail::find_timeout, Events>>::cond;
        }
        else if constexpr (volatile_timeout)
        {
            return ct::get_t<ct::index<0>, ct::filter_t<detail::find_timeout, Events>>::cond();
        }
    }

    bool wait(Bitset &bitset)
    {
        wait_<0, Es...>(bitset);
        return bitset.any();
    }

    void handle(Bitset &bitset, std::unique_lock<std::mutex> &lock, bool timeout)
    {
        handle_<0, Es...>(bitset, lock, timeout);
        bitset.reset();
    }

private:
    template<std::size_t B, typename E, typename ...Tail>
    void wait_(Bitset &bitset)
    {
        if constexpr (E::volatile_timeout || E::constant_timeout)
        {
            if constexpr (sizeof ... (Tail)) wait_<B, Tail...>(bitset);
        }
        else
        {
            bitset[B] = E::cond();
            if constexpr (sizeof ... (Tail)) wait_<B + 1, Tail...>(bitset);
        }
    }

    template<std::size_t B, typename E, typename ...Tail>
    void handle_(Bitset const &bitset, std::unique_lock<std::mutex> &lock, bool timeout)
    {
        if constexpr (E::volatile_timeout || E::constant_timeout)
        {
            if(timeout) E::action(lock);
            if constexpr (sizeof ... (Tail)) handle_<B, Tail...>(bitset, lock, timeout);
        }
        else
        {
            if(bitset[B]) E::action(lock);
            if constexpr (sizeof ... (Tail)) handle_<B + 1, Tail...>(bitset, lock, timeout);
        }
    }
};

template<typename ...T>
Handler(T&&...) -> Handler<T...>;

template<typename C, typename A>
Event(C &&, A&&) -> Event<C, A>;

}
