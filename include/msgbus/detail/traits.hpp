#pragma once
#include <chrono>

namespace msgbus::detail {

template<typename T>
struct is_duration : std::false_type {};

template<class Rep, class Period>
struct is_duration<std::chrono::duration<Rep,Period>> : std::true_type {};

template<class Rep, class Period>
struct is_duration<std::chrono::duration<Rep,Period> &> : std::true_type {};

template<class Rep, class Period>
struct is_duration<std::chrono::duration<Rep,Period> const &> : std::true_type {};

template<typename T>
constexpr bool is_duration_v = is_duration<T>::value;

template<typename T>
auto is_duration_calculator(T*) -> is_duration<decltype(std::declval<T>()())>;

template<typename T>
auto is_duration_calculator(...) -> std::false_type;

template<typename T>
constexpr bool is_duration_calculator_v = decltype(is_duration_calculator<std::decay_t<T>>((std::decay_t<T>*)nullptr))::value;

template<typename T>
struct find_timeout
{
    static constexpr bool value = T::constant_timeout || T::volatile_timeout;
};

} // namespace msgbus::detail
