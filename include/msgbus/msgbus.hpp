#pragma once
#include <cg/bind.hpp>
#include <cg/component.hpp>
#include <cg/connect.hpp>

#include <msgbus/handler.hpp>
#include <msgbus/state.hpp>
#include <msgbus/tags.hpp>

#include <iostream>
#include <sstream>
#include <csignal>
#include <numeric>
#include <thread>
#include <unordered_set>
#include <unistd.h>

namespace msgbus {

struct Msgbus : cg::Component
{
    struct Impl
    {
        template<typename Base>
        struct Interface : cg::Bind<Interface, Base>
        {
            Interface(Base &&base) : cg::Bind<Interface, Base>{std::forward<Base>(base)}
            {
                std::unique_lock<std::mutex> lock{this->state.mu};
                this->state.f = this->bind(&Interface<Base>::stop);
                this->state.cv.notify_all();
            }

            template<typename I, typename R, typename S>
            int subscribe(R &remote, S &state)
            {
                auto handler = remote.handler();

                msgbus::Status volatile status;
                {
                    std::unique_lock<std::mutex> lock{state.mu};
                    if(state.status == msgbus::Unknown)
                    {
                        state.status = msgbus::Started;
                    }
                    status = msgbus::Started;
                }

                {
                    std::unique_lock<std::mutex> lock{this->state.mu};
                    this->state.states.insert(&state);
                }

                service(status, state, handler);

                {
                    std::unique_lock<std::mutex> lock{state.mu};
                    state.status = msgbus::Stopped;
                    state.cv.notify_all();
                }
                bool should_die = false;
                {
                    std::unique_lock<std::mutex> lock{this->state.mu};
                    if(this->state.states.erase(&state))
                    {
                        should_die = this->state.states.empty();
                    }
                }
                if(should_die)
                {
                    kill(getpid(), SIGUSR1);
                }
                return 0;
            }

            void stop(std::unique_lock<std::mutex> &lock)
            {
                decltype(this->state.states) states;
                states.swap(this->state.states);
                lock.unlock();

                for(auto *state : states)
                {
                    std::unique_lock<std::mutex> lock(state->mu);
                    state->status = Stopping;
                    state->cv.notify_all();
                }

                for(auto &handler : this->state.stopped)
                {
                    handler();
                }
            }

            void stopped(std::function<void()> &&functor)
            {
                std::unique_lock<std::mutex> lock{this->state.mu};
                this->state.stopped.emplace_back(std::move(functor));
            }

        private:
            template<typename Handler, typename State>
            void service(msgbus::Status volatile &status, State &state, Handler &handler, std::enable_if_t<Handler::constant_timeout, void*> = 0)
            {
                typename Handler::Bitset events;
                using Duration = std::decay_t<decltype(handler.timeout())>;

                auto next = std::chrono::time_point_cast<Duration>(std::chrono::system_clock::now());
                if constexpr (std::is_same_v<std::chrono::seconds, Duration> || std::is_same_v<std::chrono::minutes, Duration>)
                {
                    if(auto const diff = std::gcd(handler.timeout().count(), 60); diff > 1)
                    {
                        next += Duration{diff - (next.time_since_epoch().count() % diff)};
                    }
                    else
                    {
                        next += handler.timeout();
                    }
                }

                while(msgbus::Stopping != status)
                {
                    std::unique_lock<std::mutex> lock{state.mu};
                    bool expired = !state.cv.wait_until(lock, next, [&]{
                        bool stopping = (msgbus::Stopping == (status = state.status));
                        return handler.wait(events) || stopping;
                    });
                    handler.handle(events, lock, expired);

                    while(next <= std::chrono::system_clock::now())
                    {
                        next += handler.timeout();
                    }
                }
            }

            template<typename Handler, typename State>
            void service(msgbus::Status volatile &status, State &state, Handler &handler, std::enable_if_t<Handler::volatile_timeout, void*> = 0)
            {
                typename Handler::Bitset events;
                while(msgbus::Stopping != status)
                {
                    std::unique_lock<std::mutex> lock{state.mu};
                    auto const timeout = handler.timeout();
                    if (timeout == decltype(timeout)::max())
                    {
                        state.cv.wait(lock, [&]{
                            bool stopping = (msgbus::Stopping == (status = state.status));
                            return handler.wait(events) || stopping || (timeout != handler.timeout());
                        });
                        handler.handle(events, lock, false);
                    }
                    else
                    {
                        bool expired = !state.cv.wait_for(lock, timeout, [&]{
                            bool stopping = (msgbus::Stopping == (status = state.status));
                            return handler.wait(events) || stopping || (timeout != handler.timeout());
                        });
                        handler.handle(events, lock, expired);
                    }
                }
            }

            template<typename Handler, typename State>
            void service(msgbus::Status volatile &status, State &state, Handler &handler, std::enable_if_t<(!Handler::constant_timeout && !Handler::volatile_timeout), void*> = 0)
            {
                typename Handler::Bitset events;
                while(msgbus::Stopping != status)
                {
                    std::unique_lock<std::mutex> lock{state.mu};
                    state.cv.wait(lock, [&]{
                        bool stopping = (msgbus::Stopping == (status = state.status));
                        return handler.wait(events) || stopping;
                    });
                    handler.handle(events, lock, false);
                }
            }
        };
    };

    template<typename>
    struct State 
    {
        State(std::mutex *mu, std::condition_variable *cv, std::function<void(std::unique_lock<std::mutex>&)> *f)
        : mu{*mu}, cv{*cv}, f{*f} {}

        std::mutex &mu;
        std::condition_variable &cv;
        std::vector<std::function<void()>> stopped;
        std::function<void(std::unique_lock<std::mutex>&)> &f;
        std::unordered_set<msgbus::detail::StateImpl*> states;
    };

    using Ports = ct::map<ct::pair<tag::Msgbus, Impl>>;
};

template<typename ...Components>
using Clients = cg::Connect<cg::Group<Components...>, Msgbus, tag::Msgbus>;

}
