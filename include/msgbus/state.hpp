#pragma once
#include <msgbus/detail/traits.hpp>
#include <msgbus/tags.hpp>

#include <ct/apply.hpp>
#include <ct/filter.hpp>

#include <mutex>
#include <cassert>
#include <condition_variable>

namespace msgbus {

struct Msgbus;

template<typename Interface>
class State;

enum Status
{
    Unknown,
    Started,
    Stopping,
    Stopped
};

namespace detail {

template<typename>
struct make_worker;

class StateImpl
{
public:
    std::mutex mu;
    std::condition_variable cv;
    volatile Status status = Unknown;

private:
    StateImpl() = default;

    template<typename> friend class msgbus::State;
    template<typename> friend struct make_worker;
};

} // namespace detail

template<typename Interface>
class State
{
public:
    State() = default;
    State(State &&other) = default;

    template<typename Lambda>
    void notify(Lambda &&lambda)
    {
        std::unique_lock<std::mutex> lock(impl->mu);
        lambda();
        impl->cv.notify_all();
    }

    void notify(std::unique_lock<std::mutex> &lock)
    {
        assert(lock.owns_lock());
        impl->cv.notify_all();
        lock.unlock();
    }

    template<typename Condition>
    void wait(Condition &&condition)
    {
        std::unique_lock<std::mutex> lock(impl->mu);
        impl->cv.wait(lock, [&condition] {
            return condition();
        });
    }

    template<typename COD, typename FOC>
    void wait(COD &&cod, FOC &&foc)
    {
        if constexpr (detail::is_duration<COD>::value)
        {
            std::unique_lock<std::mutex> lock(impl->mu);
            impl->cv.wait_for(lock, cod, [&foc] {
                return foc();
            });
        }
        else
        {
            std::unique_lock<std::mutex> lock(impl->mu);
            impl->cv.wait(lock, [&cod] {
                return cod();
            });
            foc(impl->cv);
        }
    }

    template<typename Duration, typename Condition, typename Function>
    bool wait(Duration &&duration, Condition &&condition, Function &&function)
    {
        std::unique_lock<std::mutex> lock(impl->mu);
        if(impl->cv.wait_for(lock, duration, [&condition] {
            return condition();
        }))
        {
            function(impl->cv);
            return true;
        }
        return false;
    }

    template<typename Lambda>
    auto lock(Lambda &&lambda)
    {
        std::unique_lock<std::mutex> lock(impl->mu);
        return lambda();
    }

    void stop()
    {
        std::unique_lock<std::mutex> lock(impl->mu);
        impl->status = Stopping;
        impl->cv.notify_all();
    }

    bool is(Status status) const
    {
        return impl->status == status;
    }

    void set(Status status)
    {
        impl->status = status;
    }

private:
    std::unique_ptr<detail::StateImpl> impl{new detail::StateImpl{}};
    template<typename> friend struct detail::make_worker;
};

} // namespace msgbus

