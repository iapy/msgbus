#pragma once

#include <ct/tuple.hpp>
#include <ct/transform.hpp>

#include <msgbus/state.hpp>
#include <msgbus/tags.hpp>

namespace msgbus {
namespace detail {

template<typename I>
struct make_worker
{
    struct Impl
    {
        template<typename Base>
        struct Interface : I::template Interface<Base>
        {
            int run()
            {
                this->remote(tag::Msgbus{}).template subscribe<I>(*this, *this->state.msgbus::template State<I>::impl);
                return 0;
            }
        };
    };

    using type = ct::tuple<Impl>;
};

template<typename ...T>
struct Workers
{
    using type = ct::transform_t<make_worker, ct::tuple<T...>>;
};

} // namespace detail

template<typename ...T>
using Workers = typename detail::Workers<T...>::type;

} // namespace msgbus
