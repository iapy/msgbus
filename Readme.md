# Abstract

The `msgbus` library improves the [metacg](https://bitbucket.org/iapy/metacg) threading model by adding instrumentation for inter-thread communication within component. Additionally, it gracefully handles application termination.

## Terminology

* A **message** can be either a timer or a change in the state of a component resulting from a call to one of the component’s interfaces.
* A **worker** is an extension of a vanilla `metacg` service (i.e. thread) that manages **message** processing. These messages can trigger code execution based on a timer or when there’s a change in the component’s state.

## Example

We will make a multi-threaded consumer-producer components similar to what we've done for [metacg](https://bitbucket.org/iapy/metacg).

```c++
#include <msgbus/state.hpp>
#include <msgbus/workers.hpp>
#include <msgbus/handler.hpp>
#include <cg/component.hpp>
#include <cg/bind.hpp>
#include <iostream>
#include <vector>

namespace tag
{
    struct Consumer {};
}

template<typename T>
struct Consumer : cg::Component
{
    template<typename Resolver>
    struct Types
    {
        using type = T;
    };

    struct Worker
    {
        template<typename Base>
        struct Interface : cg::Bind<Interface, Base>
        {
            auto handler()
            {
                using Self = Interface<Base>;
                return msgbus::Handler
                {
                    msgbus::Event
                    {
                        [this]{ return !this->state.values.empty(); },
                        this->bind(&Self::consume) // could be a lambda
                    }
                };
            }
        private:
            void consume(std::unique_lock<std::mutex> &lock)
            {
                std::vector<typename Base::type> values;
                this->state.values.swap(values);
                lock.unlock();

                for(auto const &value : values)
                {
                    std::cout << "Consumed " << value << '\n';
                }
            }
        };
    };

    struct Impl
    {
        template<typename Base>
        struct Interface : cg::Bind<Interface, Base>
        {
            void consume(typename Base::type &&value)
            {
                using S = msgbus::State<Worker>;
                this->state.S::notify([this, &value]{
                    this->state.values.emplace_back(std::forward<typename Base::type>(value));
                });
            }
        };
    };

    template<typename Types>
    struct State : msgbus::State<Worker>
    {
        std::vector<typename Types::type> values;
    };

    using Ports = ct::map
    <
        ct::pair<tag::Consumer, Impl>
    >;
    using Services = msgbus::Workers<Worker>;
};
```

There are some differences from a vanilla `metacg` service:

* The `Worker` (`Service`) interface implements a mandatory method `auto handler()`.  This method returns the set of supported events and their handlers.
* The `State` class is derived from `msgbus::State<Worker>`. It is responsible for initializing and managing a `std::mutex` and a `std::condition_variable`, which are used for multi-threaded communication. When a component introduces multiple workers, the `State` class must have several base classes, one for each worker interface.
* The `Services` are represented by `msgbus::Workers<Worker>` instead of `cg::tuple<Worker>`. The `msgbus::Workers<Worker>` class wraps the `Worker` with an interface that is compatible with the `metacg`service model. Specifically, it implements an `int run()` method that waits on the conditional variable declared in a state and dispatches events.
* The `Impl` interface modifies the state within the `this->S::notify(<lambda>)`. This action locks the mutex held by `msgbus::State<Worker>`, makes the change in the `State`, and notifies the `Worker` of that change.

Let's create a `Producer` component and wrap it all in a graph:

```c++
#include <msgbus/msgbus.hpp>
#include <msgbus/service.hpp>
#include <cg/graph.hpp>

struct Producer : cg::Component
{
    template<typename Resolver>
    struct Types
    {
        using type = typename Resolver::template Types<tag::Consumer>::type;
    };

    struct Thread
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                 auto consumer = this->remote(tag::Consumer{});
                 consumer.consume(typename Base::type{});
                 return 0;
            }
        };
    };

    using Services = ct::tuple<Thread>;
};

int main()
{
    using Graph = cg::Graph<
        cg::Connect<Producer, Consumer<int>>,
        cg::Connect<Consumer<int>, msgbus::Msgbus>
    >;
    return msgbus::Service<Graph>();
}
```

* In a graph, there exists a required edge represented by `cg::Connect<Consumer<int>, msgbus::Msgbus>`. This edge connects a `Consumer` to the message bus. It’s important to note that every component implementing `msgbus::Workers<>` must be connected to `msgbus::Msgbus`. Failure to do so would result in a compile-time error.
* Instead of using `cg::Service(host)` where the `host` is a `cg::Host<Graph>` instance, we use `msgbus::Service<Graph>(...)`. This function accepts the same arguments as the `cg::Host` constructor does. Additionally, it initialises signal handlers and the mutex/conditional variable used for graceful `SIGTERM`handling.
* When multiple components need to use a message bus, you can use the shortcut `msgbus::Clients<A, B, C>` instead of explicitly writing out `cg::Connect<cg::Group<A, B, C>, msgbus::Msgbus>`.

When you execute the provided code, you’ll observe the following output:

```
Consumed 0
```

The application remains inactive until terminated because `msgbus` keeps all the threads paused until the next event occurs. However, we can implement a mechanism to trigger an application shutdown from within the consumer once it has consumed the value. For that we modify the `consume` method of a `Worker` interface:

```c++
void consume(std::unique_lock<std::mutex> &lock)
{
    std::vector<typename Base::type> values;
    this->state.values.swap(values);
    lock.unlock();

    for(auto const &value : values)
    {
        std::cout << "Consumed " << value << '\n';
    }

    using S = msgbus::State<Worker>;
    this->state.S::stop();
}
```

Now the binary exits right after it prints
```
Consumed 0
```

### Timer

When you provide an instance of `std::chrono::duration` as the first argument to an `msgbus::Event`constructor, the associated handler will be executed at regular intervals defined by that duration. Let's demonstrate this with an example:

```c++
template<typename T>
struct Consumer : cg::Component
{
    template<typename Resolver>
    struct Types
    {
        using type = T;
    };

    struct Worker
    {
        template<typename Base>
        struct Interface : cg::Bind<Interface, Base>
        {
            auto handler()
            {
                using Self = Interface<Base>;
                return msgbus::Handler
                {
                    msgbus::Event
                    {
                        std::chrono::seconds{1},
                        [this](std::unique_lock<std::mutex> &lock)
                        {
                            int tick = ++this->state.tick;
                            std::cout << "Tick = " << tick << n;
                            lock.unlock();

                            if(tick == 5)
                            {
                                using S = msgbus::State<Worker>;
                                this->state.S::stop();
                            }
                        }
                    },
                    msgbus::Event
                    {
                        [this]{ return !this->state.values.empty(); },
                        this->bind(&Self::consume)
                    }
                };
            }
        private:
            void consume(std::unique_lock<std::mutex> &lock)
            {
                std::vector<typename Base::type> values;
                this->state.values.swap(values);
                lock.unlock();

                for(auto const &value : values)
                {
                    std::cout << "Consumed " << value << n;
                }
            }
        };
    };

    struct Impl
    {
        template<typename Base>
        struct Interface : cg::Bind<Interface, Base>
        {
            void consume(typename Base::type &&value)
            {
                using S = msgbus::State<Worker>;
                this->state.S::notify([this, &value]{
                    this->state.values.emplace_back(std::forward<typename Base::type>(value));
                });
            }
        };
    };

    template<typename Types>
    struct State : msgbus::State<Worker>
    {
        int tick{0};
        std::vector<typename Types::type> values;
    };

    using Ports = ct::map
    <
        ct::pair<tag::Consumer, Impl>
    >;
    using Services = msgbus::Workers<Worker>;
};
```

* The new event handler is configured with a condition of `std::chrono::seconds{1}`, which implies that the handler will be invoked every 1 second.
* In this case, the `msgbus::Event` is created using a lambda handler instead of using binding.
* In the `msbgus::Handler` constructor, there should be **only one** timer handler.

This code produces the following output where each line appears one second later than the previous one and terminates:

```
Consumed 0
Tick = 1
Tick = 2
Tick = 3
Tick = 4
Tick = 5
```

If the interval is specified as `std::chrono::seconds` or `std::chrono::minutes`, and the greatest common divisor (GCD) of the interval count and 60 is not equal to 1, the event will align with the system clock. For example:

- If the first argument of the `Event` is `std::chrono::seconds{10}`, the handler will execute at `:00`, `:10`, `:20`, `:30`, `:40`, and `:50` every minute.
- If the first argument of the `Event` is `std::chrono::minutes{45}`, the handler will execute every 45 minutes at `:00:00`, `:15:00`, `:30:00`, and `:45:00`.
### Volatile timeout

When passing a lambda that returns an interval, the handler is executed after the interval returned by the lambda. If the lambda returns the maximum value of the interval (e.g., `::max()`), the timer stops.

```c++
struct Component : cg::Component
{
	struct Worker
	{
		template<typename Base>
		struct Interface : cg::Bind<Interface, Base>
		{
			auto handler()
			{
				using Self = Interface<Base>;
				return msgbus::Handler
				{
					msgbus::Event
					{
						[this] { return this->state.interval; }
						this->bind(&Self::timer)
					}
				};
			}
		private:
			void timer(auto &lock)
			{
				// do something useful
			}
		};
	};

	struct Driver
	{
		template<typename Base>
		struct Interface : Base
		{
			void start(std::chrono::seconds interval)
			{
				this->state.notify([&interval, this]{
					this->state.interval = interval;
				});
			}

			void stop()
			{
				this->state.notify([this]{
					this->state.interval = std::chrono::seconds::max();
				});
			}
		};
	};

	template<typename>
	struct State
	{
		std::chrono::seconds interval = std::chrono::seconds::max();
	};

	using Ports = ct::map<ct::pair<tag::Driver, Driver>>;
    using Services = msgbus::Workers<Worker>;
};
```

To initiate, modify, and stop the timer from outside the component, we utilise the following code:

```c++

this->remote(tag::Driver{}).start(std::chrono::seconds(10));
this->remote(tag::Driver{}).start(std::chrono::seconds(20));
this->remote(tag::Driver{}).stop();
this->remote(tag::Driver{}).start(std::chrono::seconds(30));
this->remote(tag::Driver{}).stop();
```

Worker threads have the capability to independently manage their own timers:

```c++
	void timer(auto &lock)
	{
		// my job here is done, wait till someone wakes me up
		this->state.interval = std::chrono::seconds::max();
	}
```

### Handling `sigterm`

Handling the `SIGTERM` signal involves adding the following code to a `msgbus::Handler` constructor. When a `SIGTERM` signal is received, `msgbus` initiates the shutdown process by broadcasting a `msgbus::Stopping`state change to all workers.

```c++
    msgbus::Event
    {
        [this]{ return this->state.S::is(msgbus::Stopping); },
        this->bind(&Self::terminate)
    }
```

#### Non-msgbus workers

Using the following code, you can subscribe to a `sigterm` event from the vanilla `metacg` `Service`:

```c++
template<typename Base>
struct Interface : Base
{
    int run()
    {
        std::atomic<bool> stopping{false};
        this->remote(msgbus::tag::Msgbus{}).stopped([&stopping]{
            stopping = true;
        });
        // your vanilla service logic here
        return 0;
    }
};
```
